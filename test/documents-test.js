// https://glebbahmutov.com/blog/how-to-correctly-unit-test-express-server/

const express = require("express");
const database = require("../routes/database");
const request = require("supertest"); // https://github.com/visionmedia/supertest#api
                                      // https://visionmedia.github.io/superagent/
const assert = require("assert");
const fse = require("fs-extra"); // https://github.com/jprichardson/node-fs-extra

describe("/documents", () => {
    let app, db;
    const now = new Date();

    beforeEach(() => {
        db = database.open("./test/db", ":memory:", true);
        const documentRouter = require("../routes/documents")(database);
        app = express();
        app.use("/documents", documentRouter);
    });

    afterEach(() => {
        database.close();
        fse.removeSync(database.filesDir());
        fse.removeSync(database.uploadDir());
    });

    describe("GET /", () => {
        it("returns an empty list", (done) => {
            request(app)
                .get("/documents")
                .expect(200, [])
                .end(done);
        });
        it("returns a single document", (done) => {
            createDocument();
            request(app)
                .get("/documents")
                .expect(200, [{id: "an id", author: "an author", date: now.toISOString(), title: "a title"}])
                .end(done);
        });
    });

    describe("POST /", () => {
        it("handles no data", (done) => {
            request(app)
                .post("/documents")
                .expect(400, "author missing")
                .end(done);
        });
        it("handles missing data", (done) => {
            request(app)
                .post("/documents")
                .send({author: "an author"})
                .expect(400, "title missing")
                .end(done);
        });
        it("creates a document", (done) => {
            const agent = request(app);
            agent.post("/documents")
                .send({author: "an author", title: "a title"})
                .expect(201, "Created")
                .expect("location", /http:\/\/127.0.0.1\/documents\/.+/)
                .end(() => {
                    agent.get("/documents")
                        .then(res => {
                            assert.equal(res.body.length, 1);
                            assert.equal(res.body[0].author, "an author");
                            done();
                        });
                });
        });
    });

    describe("GET /:id", () => {
        it("handles an invalid ID", (done) => {
            request(app)
                .get("/documents/foo")
                .expect(404, {})
                .end(done);
        });
        it("returns a document by its ID", (done) => {
            createDocument();
            request(app)
                .get("/documents/an%20id")
                .expect(200, {
                    id: "an id",
                    author: "an author",
                    date: now.toISOString(),
                    title: "a title",
                    mimetype: "a mime type",
                    path: "a path"
                })
                .end(done);
        });
    });

    describe("PUT /:id", () => {
        it("handles missing data", (done) => {
            request(app)
                .put("/documents/foo")
                .send({author: "an author"})
                .expect(400, "title missing")
                .end(done);
        });
        it("handles an invalid ID", (done) => {
            request(app)
                .put("/documents/foo")
                .send({author: "an author", title: "a title"})
                .expect(404, {})
                .end(done);
        });
        it("updates a document", (done) => {
            createDocument();
            const agent = request(app);
            agent.put("/documents/an%20id")
                .send({author: "changed author", title: "changed title"})
                .expect(204, {})
                .end(() => {
                    agent.get("/documents/an%20id")
                        .expect(200, {
                            id: "an id",
                            author: "changed author",
                            date: now.toISOString(),
                            title: "changed title",
                            mimetype: "a mime type",
                            path: "a path"
                        })
                        .end(done);
                });
        });
    });

    describe("PUT /:id/content", () => {
        it("uploads an image", (done) => {
            createDocument(null);
            const agent = request(app);
            agent.put("/documents/an%20id/content")
                .attach("sample", "./test/sample.jpg")
                .expect(204, {})
                .end(() => {
                    agent.get("/documents/an%20id")
                        .expect(200, {
                            id: "an id",
                            author: "an author",
                            date: now.toISOString(),
                            title: "a title",
                            mimetype: "image/jpeg",
                            path: `./test/db/files/${now.getFullYear()}/${("0" + (now.getMonth() + 1)).slice(-2)}/an id-sample.jpg`
                        })
                        .end(done);
                });
        });

        it("uploads to a nonexisting document", (done) => {
            request(app)
                .put("/documents/an%20id/content")
                .attach("sample", "./test/sample.jpg")
                .expect(404, {})
                .end(done);
        });

        it("uploads a content twice", (done) => {
            createDocument();
            request(app)
                .put("/documents/an%20id/content")
                .attach("sample", "./test/sample.jpg")
                .expect(409, {})
                .end(done);
        });
    });

    function createDocument(path = "a path") {
        db.serialize(() => {
            db.run("insert into doc_documents (id, author, date, title, mimetype, path) values (?, ?, ?, ?, ?, ?)",
                ["an id", "an author", now.toISOString(), "a title", "a mime type", path]);
        });
    }
});
