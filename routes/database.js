const sqlite3 = require("sqlite3").verbose(); // https://github.com/mapbox/node-sqlite3/wiki
const fs = require("fs"); // https://nodejs.org/api/fs.html

// tutorial: http://www.sqlitetutorial.net/sqlite-nodejs/
// example: https://magnealvheim.wordpress.com/2017/05/12/creating-a-node-js-webserver-with-sqlite/
// example (including db migration): https://medium.com/@tarkus/node-js-and-sqlite-for-rapid-prototyping-bc9cf1f26f10

const database = module.exports;
let db;
let uploadDir;
let filesDir;

/** Opens an SQLITE database (backed by the given file and optionally initializing it) and returns it */
database.open = function (basedir = "./db", filename = "tstack-storage.db", create = false) {
    if (!fs.existsSync(basedir)) {
        fs.mkdirSync(basedir);
    }
    filesDir = `${basedir}/files`;
    if (!fs.existsSync(filesDir)) {
        fs.mkdirSync(filesDir);
    }
    uploadDir = `${basedir}/upload`;
    if (!fs.existsSync(uploadDir)) {
        fs.mkdirSync(uploadDir);
    }
    const openMode = create ? sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE : sqlite3.OPEN_READWRITE;
    const databaseFile = filename === ":memory:" ? filename : `${basedir}/${filename}`;
    console.log(`Opening database ${databaseFile}...`);
    db = new sqlite3.Database(databaseFile, openMode, (err) => {
        if (err) {
            throw new Error(`Could not open database ${databaseFile}: ${err.stack}`);
        }
        console.log(`Database ${databaseFile} ready`);
    });
    if (create) {
        db.serialize(() => {
            db.run("create table doc_documents (id text not null primary key, author text, date text not null, title text not null, mimetype text, path text)");
            // TODO db.run("create table doc_tags (document_id text not null, tags text, foreign key(document_id) references doc_documents(id))");
        });
    }
    return db;
};

/** Returns the SQLITE database */
database.get = function () {
    return db;
};

database.uploadDir = function () {
    return uploadDir;
};

database.filesDir = function () {
    return filesDir;
};

/** Closes the SQLITE database */
database.close = function () {
    if (db) {
        console.log(`Closing database...`);
        db.close((err) => {
            if (err) {
                console.log(`Could not close database: ${err.stack}`);
            } else {
                console.log(`Database closed`);
            }
        });
    }
};
