const express = require("express"); // http://expressjs.com/en/4x/api.html
const uuid = require("uuid/v1");
const fs = require("fs"); // https://nodejs.org/api/fs.html
const formidable = require("formidable"); // https://github.com/felixge/node-formidable
// possible alternatives to formidable: express-fileupload, multer. see https://npmcompare.com/compare/express-fileupload,formidable,multer,multiparty and https://github.com/richardgirges/express-fileupload/issues/69

module.exports = function (database) {
    console.log(`Registering documents endpoints...`);

    const db = database.get();
    const router = express.Router();
    const jsonParser = express.json();

    router.get('/', (req, res) => {
        db.all("select id, author, date, title from doc_documents", (err, rows) => {
            if (err) {
                console.log(`GET / - Could not execute SQL: ${err.stack}`);
                return res.status(500).send(`Could not execute SQL: ${err.message}`);
            }
            console.log(`GET / - Returning ${rows.length} datasets`);
            res.json(rows);
        });
    });

    router.post('/', jsonParser, (req, res) => {
        if (!req.body.author) {
            return res.status(400).send("author missing");
        }
        if (!req.body.title) {
            return res.status(400).send("title missing");
        }
        console.log(`Creating dataset based on ${JSON.stringify(req.body)}...`);
        const id = uuid();
        const stmt = db.prepare("insert into doc_documents (id, author, date, title) values (?, ?, ?, ?)", (err) => {
            if (err) {
                return res.status(500).send(`Could not prepare SQL: ${err.message}`);
            }
            stmt.run([id, req.body.author, new Date().toISOString(), req.body.title], (err) => {
                if (err) {
                    console.log(`GET / - Could not execute SQL: ${err.stack}`);
                    return res.status(500).send(`Could not execute SQL: ${err.message}`);
                }
                console.log(`A row has been inserted with rowid ${stmt.lastID}`);
                res.location(`${req.protocol}://${req.hostname}${req.originalUrl}/${id}`); // TODO include port in URL
                res.sendStatus(201);
                stmt.finalize();
            });
        });
    });

    router.get('/:id', (req, res) => {
        loadDocument(req.params.id, (err, row) => {
            if (err) {
                console.log(`GET /:id - Could not execute SQL: ${err.stack}`);
                res.status(500).send(`Could not execute SQL: ${err.message}`);
            } else if (row) {
                console.log(`GET /:id - Returning dataset`);
                res.json(row);
            } else {
                console.log(`GET /:id - Dataset not found`);
                res.sendStatus(404);
            }
        });
    });

    router.put("/:id", jsonParser, (req, res) => {
        if (!req.body.author) {
            return res.status(400).send("author missing");
        }
        if (!req.body.title) {
            return res.status(400).send("title missing");
        }
        console.log(`Updating dataset '${req.params.id}' based on ${JSON.stringify(req.body)}...`);
        const stmt = db.prepare("update doc_documents set author=?, title=? where id=?", (err) => {
            if (err) {
                return res.status(500).send(`Could not prepare SQL: ${err.message}`);
            }
            stmt.run([req.body.author, req.body.title, req.params.id], (err) => {
                if (err) {
                    console.log(`Could not execute SQL: ${err.stack}`);
                    res.status(500).send(`Could not execute SQL: ${err.message}`);
                } else if (stmt.changes === 0) {
                    console.log(`Document '${req.params.id}' not found`);
                    res.sendStatus(404);
                } else if (stmt.changes === 1) {
                    console.log(`Document '${req.params.id}' updated`);
                    res.sendStatus(204);
                } else {
                    console.log(`Unexpected number of changed rows: ${stmt.changes}`);
                    res.status(500).send(`Unexpected number of changed rows: ${stmt.changes}`);
                }
                stmt.finalize();
            });
        });
    });

    router.get('/:id/content', (req, res) => {
        res.status(500).send(`Not implemented`); // TODO implement attachments
    });

    router.put("/:id/content", jsonParser, (req, res) => {
        loadDocument(req.params.id, (err, row) => {
            if (err) {
                console.log(`GET /:id/content - Could not execute SQL: ${err.stack}`);
                res.status(500).send(`Could not execute SQL: ${err.message}`);
            } else if (!row) {
                console.log(`GET /:id/content - Dataset not found`);
                res.sendStatus(404);
            } else if (row.path) {
                console.log(`GET /:id/content - Content already exists`);
                res.status(409).send("Content already exists");
            } else {
                const form = new formidable.IncomingForm();
                form.uploadDir = database.uploadDir();
                form.keepExtensions = true;
                form.parse(req, (err, fields, files) => {
                    if (!files) {
                        return res.status(400).send("uploaded file missing");
                    }
                    const filenames = Object.keys(files);
                    if (filenames.length !== 1) {
                        return res.status(400).send("multiple files not supported");
                    }
                    const file = files[filenames[0]];
                    console.log(`Uploaded ${file.size} bytes of ${file.name} (${file.type}) to ${file.path}`);
                    const dateParts = row.date.split("-");
                    const year = dateParts[0];
                    const month = dateParts[1];
                    fs.mkdir(`${database.filesDir()}/${year}`, (err) => {
                        if (err && err.code !== "EEXIST") {
                            console.log(`Could not create directory: ${err.stack}`);
                            return res.status(500).send(`Could not create directory: ${err.message}`);
                        }
                        fs.mkdir(`${database.filesDir()}/${year}/${month}`, (err) => {
                            if (err && err.code !== "EEXIST") {
                                console.log(`Could not create directory: ${err.stack}`);
                                return res.status(500).send(`Could not create directory: ${err.message}`);
                            }
                            const targetPath = `${database.filesDir()}/${year}/${month}/${req.params.id}-${file.name}`;
                            fs.access(targetPath, fs.constants.F_OK, (err) => {
                                if (!err) {
                                    console.log(`GET /:id/content - File already exists`);
                                    return res.status(500).send("File already exists");
                                }
                                fs.rename(file.path, targetPath, (err) => {
                                    if (err) {
                                        console.log(`Could not move file: ${err.stack}`);
                                        return res.status(500).send(`Could not move file: ${err.message}`);
                                    }
                                    console.log(`Stored ${file.size} bytes to ${targetPath}`);
                                    const stmt = db.prepare("update doc_documents set mimetype=?, path=? where id=?", (err) => {
                                        if (err) {
                                            return res.status(500).send(`Could not prepare SQL: ${err.message}`);
                                        }
                                        stmt.run([file.type, targetPath, req.params.id], (err) => {
                                            if (err) {
                                                console.log(`Could not execute SQL: ${err.stack}`);
                                                res.status(500).send(`Could not execute SQL: ${err.message}`);
                                            } else if (stmt.changes === 0) {
                                                console.log(`Document '${req.params.id}' not found`);
                                                res.sendStatus(404);
                                            } else if (stmt.changes === 1) {
                                                console.log(`Document '${req.params.id}' updated`);
                                                res.sendStatus(204);
                                            } else {
                                                console.log(`Unexpected number of changed rows: ${stmt.changes}`);
                                                res.status(500).send(`Unexpected number of changed rows: ${stmt.changes}`);
                                            }
                                            stmt.finalize();
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            }
        });
    });

    function loadDocument(id, callback) {
        db.get("select id, author, date, title, mimetype, path from doc_documents where id=?", [id], callback);
    }

    return router;
};
