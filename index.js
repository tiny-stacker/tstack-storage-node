const express = require('express');
const port = 5050;

const database = require("./routes/database");

const app = express();
database.open();
const documents = require("./routes/documents")(database);
app.use('/documents', documents);

const server = app.listen(port, () => console.log(`Listening on port ${port}`));

process.on('SIGINT', () => {
    console.log("Shutting down...");
    database.close();
    server.close((err) => {
        if (err) {
            console.log(`Could not shut down: ${err.stack}`);
        } else {
            console.log("Shutdown complete");
        }
    });
});
